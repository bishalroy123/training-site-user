package com.api.v1.training.user.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.repository.UserProfileRepository;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserProfileRepository userProfileRepository;

	@Transactional
	@Override
	public void register(CustomerRequest request) {
		
		User user=request.getUser();
		user.setStatus("P");
		userRepository.save(user);
		Userprofile userProfile=request.getUserprofile();
		userProfile.setProfileupdatestatus("P");
		//userProfile.setUserid(user.getUserid()); // please update as per entity
		Integer profileId=findMaxProfileId(user.getUserid());
		// userProfile.setProfileversion(profileId); please update as per entity
		userProfileRepository.save(userProfile);
		user.setProfileversion(profileId);
		userRepository.save(user);
	}
	
	private Integer findMaxProfileId(Integer userId){
	List<Userprofile> userProfile=	userProfileRepository.findByuseridOrderByProfileversionAsc(userId);
	if(!userProfile.isEmpty()){
		 
		// Please update as per DB structure
		//return userProfile.get(0).getProfileversion()+1;
		return 0;
	}
	else{
		return 1;
		}
	}
	
	public List<User> getAllUsers() {
	      List<User> customer = userRepository.findAll();
	      return customer;
    } 

	@Override
	public User findById(int userId) {
		User user = userRepository.getOne(userId);
    	return user;
	}

}
