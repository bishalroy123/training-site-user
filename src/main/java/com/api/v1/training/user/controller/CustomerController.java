package com.api.v1.training.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.response.ResponseData;
import com.api.v1.training.user.response.ResponseWrapper;
import com.api.v1.training.user.service.CustomerService;

@SpringBootApplication
@RestController
@RequestMapping("trainingsite/user")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	           
    @RequestMapping("/{id}")
    public ResponseEntity<User> checkCustAcctStatus(@PathVariable(value = "id") int userId){
      User user = customerService.findById(userId);
      return ResponseEntity.ok().body(user);
    }
    
    @PostMapping()
	public ResponseData registerCustomer(@RequestBody CustomerRequest request) {
		customerService.register(request);
		return new ResponseData(HttpStatus.OK,"Registred Successfully");
	}

	@PutMapping(value = "/{id}")
	//public ResponseWrapper<User> updateCustomerProfile(@Valid @RequestBody User user) {   please update its erro
	public ResponseWrapper<User> updateCustomerProfile(@RequestBody User user) {
		return new ResponseWrapper("Updated Profile Successfully", HttpStatus.OK);
	}
    
}