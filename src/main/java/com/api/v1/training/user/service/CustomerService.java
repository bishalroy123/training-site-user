package com.api.v1.training.user.service;

import java.util.List;

import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.request.CustomerRequest;

/*
@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	
    public List<Customer> getAllUsers() {
      List<Customer> customer = customerRepository.findAll();
      return customer;
    } 
	
	
    public Customer findById(long userId) {
      Customer customer = customerRepository.findOne(userId);
      return customer;
    }
 }
 
*/
public interface CustomerService {

	void register(CustomerRequest request);
	
	public List<User> getAllUsers();
	
	public User findById(int userId) ;

}
	
    

