package com.api.v1.training.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.api.v1.training.user.domain.Userprofile;

public interface UserProfileRepository extends JpaRepository<Userprofile, Integer> {

	
	List<Userprofile> findByuserid(Integer userId);
	
	
	List<Userprofile> findByuseridOrderByProfileversionAsc(Integer userId);
//	@Query("FROM userprofile u WHERE u.usrid = ?1 order by desc limit 1")
//	Optional<UserProfileModel>  findFirstByOrderByprofileidDesc(Integer userId);
}
